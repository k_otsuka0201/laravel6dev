<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    public function player()
    {
        return $this->belongsTo('App\Player');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }
}
