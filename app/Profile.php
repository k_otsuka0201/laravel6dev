<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    public function player()
    {
        return $this->belongsTo('App\Player');
    }
}
