<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    public function profile()
    {
        return $this->hasOne('App\Profile');
    }

    public function photos()
    {
        return $this->hasMany('App\Photo');
    }
}
