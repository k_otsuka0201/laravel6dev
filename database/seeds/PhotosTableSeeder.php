<?php

use Illuminate\Database\Seeder;

class PhotosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $photos = [
            [
                1,
                '1.jpg',
            ],
            [
                1,
                '2.jpg',
            ],
            [
                1,
                '3.jpg',
            ],
            [
                2,
                '4.jpg',
            ],
            [
                2,
                '5.jpg',
            ],
        ];

        $now = date("Y/m/d H:i:s", time());

        foreach ($photos as $photo) {
            DB::table('photos')->insert([
                'player_id' => $photo[0],
                'file_name' => $photo[1],
                'created_at' => $now,
                'updated_at' => $now,
            ]);
        }
    }
}
