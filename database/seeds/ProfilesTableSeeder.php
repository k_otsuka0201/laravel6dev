<?php

use Illuminate\Database\Seeder;

class ProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $profiles = [
            [
                1,
                'Trump',
                'Donald',
            ],
            [
                2,
                'Clinton',
                'Hillary',
            ],
        ];

        $now = date("Y/m/d H:i:s", time());

        foreach ($profiles as $profile) {
            DB::table('profiles')->insert([
                'player_id' => $profile[0],
                'last_name' => $profile[1],
                'first_name' => $profile[2],
                'created_at' => $now,
                'updated_at' => $now,
            ]);
        }
    }
}
