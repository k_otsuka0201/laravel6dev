<?php

use Illuminate\Database\Seeder;

class PlayersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $players = [
            'Donald',
            'Hillary',
        ];

        $now = date("Y/m/d H:i:s", time());

        foreach ($players as $player) {
            DB::table('players')->insert([
                'name' => $player,
                'created_at' => $now,
                'updated_at' => $now,
            ]);
        }
    }
}
