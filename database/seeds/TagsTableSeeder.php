<?php

use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tags = [
            'animal',
            'flower',
            'human',
        ];

        $now = date("Y/m/d H:i:s", time());

        foreach ($tags as $tag) {
            DB::table('tags')->insert([
                'tag_name' => $tag,
                'created_at' => $now,
                'updated_at' => $now,
            ]);
        }
    }
}
