<?php

use Illuminate\Database\Seeder;

class PhotoTagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $photo_tags = [
            [
                1,
                1,
            ],
            [
                1,
                2,
            ],
            [
                1,
                3,
            ],
            [
                3,
                3,
            ],
            [
                4,
                1,
            ],
            [
                4,
                2,
            ],
            [
                5,
                1,
            ],
            [
                5,
                3,
            ],
        ];

        $now = date("Y/m/d H:i:s", time());

        foreach ($photo_tags as $photo_tag) {
            DB::table('photo_tag')->insert([
                'photo_id' => $photo_tag[0],
                'tag_id' => $photo_tag[1],
                'created_at' => $now,
                'updated_at' => $now,
            ]);
        }
    }
}
